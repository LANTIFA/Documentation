Hardware
=============

Some devices are more special than others. To administrate e.g. a Storagedevice you need a good documentation.

## Server

* PRIMERGY RX300 S4
    * Function: Hypervisor at the Gasthof
    * Links
        * [Datasheet (en)](https://www.fujitsu.com/downloads/PRMRGY/sys-con-rx300s4.pdf)
        * [Manual (en)](https://www.fujitsu.com/downloads/PRMRGY/b7fh-5841-01en.pdf)

## WLAN-Router

* Aruba AP-175P
    * Links
        * [Manual (en)](https://www.arubanetworks.com/assets/og/OG_AP-175.pdf)
